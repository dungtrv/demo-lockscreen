package com.mehuljoisar.lockscreen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.mehuljoisar.lockscreen.service.LockscreenService;

public class LockScreenActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_lockscreen);

        startService(new Intent(this, LockscreenService.class));

    }


}