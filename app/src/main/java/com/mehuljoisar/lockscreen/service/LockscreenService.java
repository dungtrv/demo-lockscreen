package com.mehuljoisar.lockscreen.service;

import android.app.KeyguardManager;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

import com.mehuljoisar.lockscreen.broadcast.LockscreenIntentReceiver;

public class LockscreenService extends Service {

    private LockscreenIntentReceiver mReceiver;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @SuppressWarnings("deprecation")
    public void onCreate() {



        super.onCreate();
    }

    // Register for Lockscreen event intents
    @SuppressWarnings("deprecation")
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
        KeyguardManager.KeyguardLock key;
        KeyguardManager km = (KeyguardManager)getSystemService(KEYGUARD_SERVICE);

        //This is deprecated, but it is a simple way to disable the lockscreen in code
        key = km.newKeyguardLock("IN");

        key.disableKeyguard();

        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_BOOT_COMPLETED);

        mReceiver = new LockscreenIntentReceiver();
        registerReceiver(mReceiver, filter);
		return START_STICKY;
	}

//	// Run service in foreground so it is less likely to be killed by system
//	private void startForeground() {
//		Notification notification = new NotificationCompat.Builder(this)
//		 .setContentTitle(getResources().getString(R.string.app_name))
//		 .setTicker(getResources().getString(R.string.app_name))
//		 .setContentText("Running")
//		 .setSmallIcon(R.drawable.ic_launcher)
//		 .setContentIntent(null)
//		 .setOngoing(true)
//		 .build();
//		 startForeground(9999,notification);
//	}

    // Unregister receiver
    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
    }
}
