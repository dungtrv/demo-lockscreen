package com.mehuljoisar.lockscreen.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import com.mehuljoisar.lockscreen.R;

/**
 * Created by NguyenDat on 7/19/16.
 */
public class MainService extends Service implements View.OnClickListener {

    private View view;
    private LayoutInflater layoutInflater;
    private Button btnUnlock;
    private WindowManager windowManager;
    private WindowManager.LayoutParams params;
    private WebView webView;
    private Handler handler;
    private int uiOptions;
    private TestThread testThread;

    private class TestThread implements Runnable {

        @Override
        public void run() {
            view.setSystemUiVisibility(uiOptions);
            handler.postDelayed(this, 0);
        }
    }

    public MainService() {
        testThread = new TestThread();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        handler = new Handler();
        layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.main_service, null);
        btnUnlock = (Button) view.findViewById(R.id.btn_unlock);
        webView = (WebView) view.findViewById(R.id.web_view);
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
            }


        });
        webView.loadUrl("http://dantri.com.vn/");
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        params = new WindowManager.LayoutParams();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        params.type = WindowManager.LayoutParams.TYPE_PHONE; // << TODO IMPORTANT
        /***Style 1*/
        params.flags = WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
                | WindowManager.LayoutParams.FLAG_FULLSCREEN
                | WindowManager.LayoutParams.FLAG_DIM_BEHIND
                | WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON;

        uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        view.setSystemUiVisibility(uiOptions);
        view.setFocusable(false);
        view.setClickable(false);
        view.setOnTouchListener(null);
        view.setOnClickListener(null);
        view.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    handler.post(testThread);
                    Log.e("MainService", "vao roi em ei");
                }
            }
        });

        params.format = -3;
        addLock();
        btnUnlock.setOnClickListener(this);
    }


    private void addLock() {
        if (windowManager != null && params != null) {
            windowManager.addView(view, params);
        }
    }

    @Override
    public void onClick(View v) {
        removeLock();
    }

    private void removeLock() {
        if (windowManager != null && params != null) {
            stopSelf();
            windowManager.removeView(view);
        }
    }

    @Override
    public void onDestroy() {
        removeLock();
        handler.removeCallbacks(testThread);
        super.onDestroy();
    }
}
