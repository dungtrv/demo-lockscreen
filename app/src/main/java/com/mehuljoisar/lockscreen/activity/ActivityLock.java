package com.mehuljoisar.lockscreen.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.mehuljoisar.lockscreen.R;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by NguyenDat on 7/19/16.
 */
public class ActivityLock extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        final View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(uiOptions);
        decorView.setOnSystemUiVisibilityChangeListener
                (new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {

                        Timer timer = new Timer();
                        TimerTask task = new TimerTask() {
                            @Override
                            public void run() {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        decorView.setSystemUiVisibility(uiOptions);
                                    }
                                });
                            }
                        };

                        timer.scheduleAtFixedRate(task, 0, 1);
                    }
                });
        setContentView(R.layout.activity_lock);

    }

    @Override
    public void onBackPressed() {
        return;
    }

}
